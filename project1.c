#include <stdio.h>

// Real password as well as several fake passwords to be stored in the .data segment for slight obfuscation
// Will make getting the password a little less trivial. But still attainable for a first exercise
const char* password[9] = { 
	"HARLEY",  
	"EKANS2020!",
	"DODGE_THIS",
	"5up3r53cr37p455",
	"test",
	"DOLLY_VARDEN",
	"DiStaNTFronTIeR",
	"dinar",
	"DistantRunnerALB" 
};

int ValidatePassword(char* aPassword)
{
	char* realPass = password[1];
	int invalid = strcspn(aPassword, realPass);
	
	if(invalid)
	{
		return 0;
	}
	else
	{
		return 1;
	}
}

int main()
{
	char password[50];
	printf("Hello Agent! Welcome to the organization.\n\n");
	printf("First things first, we need to validate your credentials before you can proceed. We know you have entered them one too many times, but please bear with us while we get you inprocessed.\n");
	printf("Password:");
	
	fgets(password, 50, stdin);

	if (ValidatePassword(password))
	{
		printf("\nAccess granted! Welcome, agent Hareem.\n");
		printf("Please await further orders to gain access to the corporate media hub.\n");
	}
	else
	{
		printf("\nINVALID! Cannot proceed with induction until your identity is validated.\n");
	}

    return 0;
}
